#include <iostream>
#include <iomanip>
#include <nxLib.h>
#include "utility.h"
#define PATTERN_NUM 12

using std::cout;
using std::endl;
using std::setw;

int main()
{
	// Initialize NxLib and enumerate cameras
	nxLibInitialize(true);

	// Reference to the first camera in the node BySerialNo
	NxLibItem root;
	NxLibItem camera = root[itmCameras][0];

	// Open the Ensenso
	NxLibCommand open(cmdOpen);
	open.parameters()[itmCameras] = camera[itmSerialNumber].asString();
	open.execute();

	//Set Pattern grid spacing
	root[itmCalibration][itmPattern][itmGridSpacing] = 11.25;
	cout << "Grid Spacing : " << root[itmCalibration][itmPattern][itmGridSpacing].asDouble() << "mm" << endl << endl;

	//Workspace Calibration
	EnsensoWorkspaceCalibration(camera);
	cout << "Finish Workspace Calibration" << endl;
	getchar();

	/*Eye to Hand Calibration*/
	// Discard any pattern observations that might already be in the pattern buffer
	NxLibCommand(cmdDiscardPatterns).execute();
	cout << "Discard any pattern in the pattern buffer" << endl << endl;

	// Turn off the camera's projector so that we can observe the calibration pattern
	camera[itmParameters][itmCapture][itmProjector] = false;
	camera[itmParameters][itmCapture][itmFrontLight] = true;
	cout << "Turn off the camera's projector" << endl << endl;

	//Observe the pattern 12 times
	for (int i = 0; i < PATTERN_NUM; i++)
	{
		cout << "If manipulator is at right poistion, please press \"Enter\"" << endl;
		cout << "Count : " << i + 1 << endl;
		getchar();
		

		NxLibCommand capture(cmdCapture);
		capture.parameters()[itmCameras] = camera[itmSerialNumber].asString();
		capture.execute();
		bool findPattern = false;
		try
		{
			NxLibCommand collectPattern(cmdCollectPattern);
			collectPattern.parameters()[itmCameras] = camera[itmSerialNumber].asString();
			collectPattern.execute();
			findPattern = true;
		}
		catch(NxLibException&) {}

		if (findPattern)
		{
			cout << "Find Pattern" << endl << endl;
			getchar();
		}
		else
		{
			cout << "Didn't Find Pattern" << endl << endl;
			getchar();
		}

		system("cls");
	}

	//Estimate Pattern Pose
	NxLibCommand estimatePatternPose(cmdEstimatePatternPose);
	estimatePatternPose.parameters()[itmAverage] = false;
	estimatePatternPose.execute();

	vector<POSE> patternPose;
	for (int i = 0; i < PATTERN_NUM; i++)
	{
		POSE tmp_patternPose;
		tmp_patternPose.angle = estimatePatternPose.result()[itmPatterns][i][itmPatternPose][itmRotation][itmAngle].asDouble();
		tmp_patternPose.axis_x = estimatePatternPose.result()[itmPatterns][i][itmPatternPose][itmRotation][itmAxis][0].asDouble();
		tmp_patternPose.axis_y = estimatePatternPose.result()[itmPatterns][i][itmPatternPose][itmRotation][itmAxis][1].asDouble();
		tmp_patternPose.axis_z = estimatePatternPose.result()[itmPatterns][i][itmPatternPose][itmRotation][itmAxis][2].asDouble();
		tmp_patternPose.x = estimatePatternPose.result()[itmPatterns][i][itmPatternPose][itmTranslation][0].asDouble();
		tmp_patternPose.y = estimatePatternPose.result()[itmPatterns][i][itmPatternPose][itmTranslation][1].asDouble();
		tmp_patternPose.z = estimatePatternPose.result()[itmPatterns][i][itmPatternPose][itmTranslation][2].asDouble();

		cout << tmp_patternPose.angle << " " << tmp_patternPose.axis_x << " " << tmp_patternPose.axis_y << " " << tmp_patternPose.axis_z << endl;
		cout << tmp_patternPose.x << " " << tmp_patternPose.y << " " << tmp_patternPose.z << endl;

		patternPose.push_back(tmp_patternPose);
	}

	//Save Pattern Pose
	savePatternPose(patternPose);

	//Load Tool Pose
	vector<POSE> toolPose;
	cout << "Load Tool Pose" << endl;
	loadToolPose("tool_pose.txt", toolPose);
	Quaternion2AngleAxis(toolPose);

	//Start to Hand-Eye Calibration
	cout << "Start Hand-Eye Calibration" << endl;
	NxLibCommand calibration(cmdCalibrateHandEye);
	calibration.parameters()[itmSetup] = valFixed;
	for (int i = 0; i < toolPose.size(); i++)
	{
		calibration.parameters()[itmTransformations][i][itmRotation][itmAngle] = toolPose[i].angle;
		calibration.parameters()[itmTransformations][i][itmRotation][itmAxis][0] = toolPose[i].axis_x;
		calibration.parameters()[itmTransformations][i][itmRotation][itmAxis][1] = toolPose[i].axis_y;
		calibration.parameters()[itmTransformations][i][itmRotation][itmAxis][2] = toolPose[i].axis_z;
		calibration.parameters()[itmTransformations][i][itmTranslation][0] = toolPose[i].x;
		calibration.parameters()[itmTransformations][i][itmTranslation][1] = toolPose[i].y;
		calibration.parameters()[itmTransformations][i][itmTranslation][2] = toolPose[i].z;
		cout << setw(10) << toolPose[i].angle << setw(10) << toolPose[i].axis_x << setw(10) << toolPose[i].axis_y << setw(10) << toolPose[i].axis_z << endl;
	}
	calibration.execute();

	//Show and Save the result
	cout << "Save the Result" << endl;
	POSE Camera2Base;
	Camera2Base.angle = calibration.result()[itmLink][itmRotation][itmAngle].asDouble();
	Camera2Base.axis_x = calibration.result()[itmLink][itmRotation][itmAxis][0].asDouble();
	Camera2Base.axis_y = calibration.result()[itmLink][itmRotation][itmAxis][1].asDouble();
	Camera2Base.axis_z = calibration.result()[itmLink][itmRotation][itmAxis][2].asDouble();
	Camera2Base.x = calibration.result()[itmLink][itmTranslation][0].asDouble();
	Camera2Base.y = calibration.result()[itmLink][itmTranslation][1].asDouble();
	Camera2Base.z = calibration.result()[itmLink][itmTranslation][2].asDouble();

	cout << "Angle : " << Camera2Base.angle << endl;
	cout << "Axis : " << Camera2Base.axis_x << " " << Camera2Base.axis_y << " " << Camera2Base.axis_z << endl;
	cout << "Translation : " << Camera2Base.x << " " << Camera2Base.y << " " << Camera2Base.z << endl;
	saveResult(Camera2Base);
	

	//Store the new calibration to the camera's EEPROM.
	cout << "Store the calibration result in camera's EEPROM" << endl;
	NxLibCommand storeCalibration(cmdStoreCalibration);
	storeCalibration.parameters()[itmCameras] = camera[itmSerialNumber].asString();
	storeCalibration.parameters()[itmLink] = true;
	storeCalibration.execute();


	// Close first Ensenso
	NxLibCommand close(cmdClose);
	close.parameters()[itmCameras] = camera[itmSerialNumber].asString();
	close.execute();

	nxLibFinalize();

	cout << "------------------------------------------------------------------" << endl;
	cout << "Finish" << endl;
	getchar();
	return 0;
}