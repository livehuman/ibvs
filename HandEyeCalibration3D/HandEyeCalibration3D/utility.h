#pragma once
#include <iostream>
#include <fstream>
#include <cmath>
#include <nxLib.h>

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;

struct POSE
{
	double x = 0;
	double y = 0;
	double z = 0;

	double qw = 0;
	double qx = 0;
	double qy = 0;
	double qz = 0;

	double axis_x = 0;
	double axis_y = 0;
	double axis_z = 0;
	double angle = 0;
};

void EnsensoWorkspaceCalibration(NxLibItem &camera);

void loadToolPose(string fileName, vector<POSE> &toolPose);

void Quaternion2AngleAxis(vector<POSE> &toolPose);

void savePatternPose(vector<POSE> patternPose);

void saveResult(POSE Camera2Base);