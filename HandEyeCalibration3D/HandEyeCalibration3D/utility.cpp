#include "utility.h"

void EnsensoWorkspaceCalibration(NxLibItem &camera)
{
	// Discard any pattern observations that might already be in the pattern buffer
	NxLibCommand(cmdDiscardPatterns).execute();
	cout << "Discard any pattern in the pattern buffer" << endl << endl;

	// Turn off the camera's projector so that we can observe the calibration pattern
	camera[itmParameters][itmCapture][itmProjector] = false;
	camera[itmParameters][itmCapture][itmFrontLight] = true;
	cout << "Turn off the camera's projector" << endl << endl;

	// Take some images and search for the calibration patterns in them
	for (int i = 0; i < 10; i++) {
		NxLibCommand capture(cmdCapture);
		capture.parameters()[itmCameras] = camera[itmSerialNumber].asString();
		capture.execute();
		try 
		{
			NxLibCommand collectPattern(cmdCollectPattern);
			collectPattern.parameters()[itmCameras] = camera[itmSerialNumber].asString();
			collectPattern.execute();
		}
		catch (NxLibException&) 
		{
			// Could not find any calibration patterns...
		}
	}

	// Estimate the position of the calibration pattern
	NxLibCommand estimatePatternPose(cmdEstimatePatternPose);
	estimatePatternPose.parameters()[itmAverage] = true;
	estimatePatternPose.execute();

	// Perform a workspace calibration with the estimated pattern pose as input
	NxLibCommand calibrateWorkspace(cmdCalibrateWorkspace);
	calibrateWorkspace.parameters()[itmCameras] = camera[itmSerialNumber].asString();
	calibrateWorkspace.parameters()[itmPatternPose] << estimatePatternPose.result()[itmPatternPose];
	calibrateWorkspace.execute();

	// Store the new calibration to the camera's EEPROM.
	NxLibCommand storeCalibration(cmdStoreCalibration);
	storeCalibration.parameters()[itmCameras] = camera[itmSerialNumber].asString();
	storeCalibration.parameters()[itmLink] = true;
	storeCalibration.execute();

	//Save the result
	ofstream resultFile("workspace2camera.txt");
	POSE Camera2Target;
	Camera2Target.angle = camera[itmLink][itmRotation][itmAngle].asDouble();
	Camera2Target.axis_x = camera[itmLink][itmRotation][itmAxis][0].asDouble();
	Camera2Target.axis_y = camera[itmLink][itmRotation][itmAxis][1].asDouble();
	Camera2Target.axis_z = camera[itmLink][itmRotation][itmAxis][2].asDouble();
	Camera2Target.x = camera[itmLink][itmTranslation][0].asDouble();
	Camera2Target.y = camera[itmLink][itmTranslation][1].asDouble();
	Camera2Target.z = camera[itmLink][itmTranslation][2].asDouble();

	resultFile << Camera2Target.angle << " " << Camera2Target.axis_x << " " << Camera2Target.axis_y << " " << Camera2Target.axis_z << "\n";
	resultFile << Camera2Target.x << " " << Camera2Target.y << " " << Camera2Target.z;
	resultFile.close();

}

void loadToolPose(string fileName, vector<POSE> &toolPose)
{
	ifstream posFile(fileName, ifstream::in);

	while (!posFile.eof())
	{
		POSE tmpPose;
		posFile >> tmpPose.x >> tmpPose.y >> tmpPose.z >> tmpPose.qw >> tmpPose.qx >> tmpPose.qy >> tmpPose.qz;
		toolPose.push_back(tmpPose);
	}

	posFile.close();
}

void Quaternion2AngleAxis(vector<POSE>& toolPose)
{
	for (int i = 0; i < toolPose.size(); i++)
	{
		toolPose[i].angle = 2 * acos(toolPose[i].qw);
		toolPose[i].axis_x = toolPose[i].qx / sqrt(1 - toolPose[i].qw*toolPose[i].qw);
		toolPose[i].axis_y = toolPose[i].qy / sqrt(1 - toolPose[i].qw*toolPose[i].qw);
		toolPose[i].axis_z = toolPose[i].qz / sqrt(1 - toolPose[i].qw*toolPose[i].qw);
	}
}

void savePatternPose(vector<POSE> patternPose)
{
	ofstream resultFile("patternPose.txt");
	for (int i = 0; i < patternPose.size(); i++)
	{
		resultFile << patternPose[i].angle << " " << patternPose[i].axis_x << " " << patternPose[i].axis_y << " " << patternPose[i].axis_z << endl;
		resultFile << patternPose[i].x << " " << patternPose[i].y << " " << patternPose[i].z;
		if (i < patternPose.size() - 1)
			resultFile << endl;
	}
	resultFile.close();
}

void saveResult(POSE Camera2Base)
{
	ofstream resultFile("base2camera.txt");
	resultFile << Camera2Base.angle << " " << Camera2Base.axis_x << " " << Camera2Base.axis_y << " " << Camera2Base.axis_z << "\n";
	resultFile << Camera2Base.x << " " << Camera2Base.y << " " << Camera2Base.z;
	resultFile.close();
}


