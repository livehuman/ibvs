## IEEE parts for feature
https://ieeexplore.ieee.org/abstract/document/5354493
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.330.3786&rep=rep1&type=pdf
https://ieeexplore.ieee.org/abstract/document/4273375
https://ieeexplore.ieee.org/abstract/document/6907298

http://openaccess.thecvf.com/content_CVPR_2019/papers/Yifan_Patch-Based_Progressive_3D_Point_Set_Upsampling_CVPR_2019_paper.pdf
http://openaccess.thecvf.com/content_cvpr_2018/papers/Wang_SGPN_Similarity_Group_CVPR_2018_paper.pdf
https://www.sciencedirect.com/science/article/pii/S0097849317301942

Shiu Y C, Ahmad S. Calibration of wrist-mounted robotic sensors by solving homogeneous transform equations of the form AX=XB[J]. IEEE Transactions on Robotics & Automation, 1989, 5(1):16-29.
————————————————
版权声明：本文为CSDN博主「苏源流」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/KYJL888/article/details/102456009