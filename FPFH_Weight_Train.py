import os
import gc
import math
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import keras.backend as K

import cv2

import open3d as o3

import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

from functools import partial
from fastai.vision import Path

from math import cos, acos, pi, sin, asin
from mpl_toolkits.mplot3d import Axes3D
#from pathlib import Path

from sklearn.model_selection import train_test_split

from keras.models import *
from keras.layers import *
from keras.losses import *
from keras.optimizers import *
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau


clip180 = partial(np.clip, a_min=-3.14, a_max=3.14)
clip90 = partial(np.clip, a_min=0., a_max=1.57)

def dataShift(data, core):    #使全部點剛好在第一象限
    data[:, ::3] = data[:, ::3] - core[0, 0]
    data[:, 1::3] = data[:, 1::3] - core[0, 1]
    data[:, 2::3] = data[:, 2::3] - core[0, 2]

    xShiftTmp = min(data[0, ::3])
    yShiftTmp = min(data[0, 1::3])
    zShiftTmp = min(data[0, 2::3])

    data[:, ::3] = data[:, ::3] - xShiftTmp
    data[:, 1::3] = data[:, 1::3] - yShiftTmp
    data[:, 2::3] = data[:, 2::3] - zShiftTmp
    return data

def eva_rms_with_same_points(pred, tar):
    pred_tmp = pred.reshape(-1)
    tar_tmp = tar.reshape(-1)
    assert pred_tmp.shape == tar_tmp.shape
    return np.sqrt(np.mean((pred_tmp - tar_tmp)**2))

def find_corresponding_points(pred, tar):
    pred_tmp = pred.reshape(-1)
    tar_tmp = tar.reshape(-1)
    assert pred_tmp.shape == tar_tmp.shape
    return np.sqrt(np.mean((pred_tmp - tar_tmp)**2))

def gather_pcd(pcd ,idx):
    list_pcd = []
    for index in idx:
        list_pcd.append(pcd[index])
    np_pcd = np.asarray(list_pcd)
    return np_pcd

def get_files_names(path):
    fnames=[]
    for idx, name in enumerate(path.ls()):
        if name.suffix == '.ply':
            fnames.append(name)
    return fnames

def get3DMatrix(xDis, yDis, zDis, rData, mSize):
    MCount = np.zeros((mSize, mSize, mSize))
    MCount = MCount + 1e-7    #避免除零    
    MResult = np.zeros((mSize, mSize, mSize * 3))    #三通道
    MXSum = np.zeros((mSize, mSize, mSize))
    MYSum = np.zeros((mSize, mSize, mSize))
    MZSum = np.zeros((mSize, mSize, mSize))

    for i in range (rData.shape[1] // 3):
        D1Tmp = math.floor(rData[0, i*3] / xDis * mSize)
        D2Tmp = math.floor(rData[0, i*3 + 1] / yDis * mSize)
        D3Tmp = math.floor(rData[0, i*3 + 2] / zDis * mSize)
        
        D1Tmp = np.minimum(12, D1Tmp)
        D2Tmp = np.minimum(12, D2Tmp)
        D3Tmp = np.minimum(12, D3Tmp)
        
        D1Tmp = np.maximum(-12, D1Tmp)
        D2Tmp = np.maximum(-12, D2Tmp)
        D3Tmp = np.maximum(-12, D3Tmp)

        if D1Tmp != mSize and D2Tmp != mSize and D3Tmp != mSize:
            MCount[D1Tmp, D2Tmp, D3Tmp] = MCount[D1Tmp, D2Tmp, D3Tmp] + 1
            MXSum[D1Tmp, D2Tmp, D3Tmp] = MXSum[D1Tmp, D2Tmp, D3Tmp] + rData[0, i*3]
            MYSum[D1Tmp, D2Tmp, D3Tmp] = MYSum[D1Tmp, D2Tmp, D3Tmp] + rData[0, i*3 + 1]
            MZSum[D1Tmp, D2Tmp, D3Tmp] = MZSum[D1Tmp, D2Tmp, D3Tmp] + rData[0, i*3 + 2]
        else:
            D1Tmp = D1Tmp - 1 if D1Tmp == mSize else D1Tmp
            D2Tmp = D2Tmp - 1 if D2Tmp == mSize else D2Tmp
            D3Tmp = D3Tmp - 1 if D3Tmp == mSize else D3Tmp

            MCount[D1Tmp, D2Tmp, D3Tmp] = MCount[D1Tmp, D2Tmp, D3Tmp] + 1
            MXSum[D1Tmp, D2Tmp, D3Tmp] = MXSum[D1Tmp, D2Tmp, D3Tmp] + rData[0, i*3]
            MYSum[D1Tmp, D2Tmp, D3Tmp] = MYSum[D1Tmp, D2Tmp, D3Tmp] + rData[0, i*3 + 1]
            MZSum[D1Tmp, D2Tmp, D3Tmp] = MZSum[D1Tmp, D2Tmp, D3Tmp] + rData[0, i*3 + 2]
            
    MXSum = np.divide(MXSum, MCount)
    MYSum = np.divide(MYSum, MCount)
    MZSum = np.divide(MZSum, MCount)

    for i in range(mSize):
        MResult[:, :, i * 3] = MXSum[:, :, i]
        MResult[:, :, i * 3 + 1] = MYSum[:, :, i]
        MResult[:, :, i * 3 + 2] = MZSum[:, :, i]

    return MResult    

def getAABBSize(rdata):  
    xTmp = rdata[0, ::3]
    yTmp = rdata[0, 1::3]
    zTmp = rdata[0, 2::3]

    xDis = max(xTmp) - min(xTmp)
    yDis = max(yTmp) - min(yTmp)
    zDis = max(zTmp) - min(zTmp) 

    return xDis, yDis, zDis

def getCore(data):
    coreTmp = np.zeros((1, 3))
    coreTmp[:, 0] = np.sum(data[:, ::3], axis=1) / (data.shape[1] / 3)   #coreX
    coreTmp[:, 1] = np.sum(data[:, 1::3], axis=1) / (data.shape[1] / 3)  #coreY
    coreTmp[:, 2] = np.sum(data[:, 2::3], axis=1) / (data.shape[1] / 3)  #coreZ

    return coreTmp

def getMaxRadius(data):
    maxTmp = 0
    indexTmp = 0

    for i in range(data.shape[1] // 3):
        tmp = np.sqrt(np.square(data[0, i*3]) + np.square(data[0, i*3 + 1]) + np.square(data[0, i*3 + 2]))
        if (tmp  > maxTmp):
            maxTmp = tmp
            indexTmp = i    

    return indexTmp, maxTmp

def invQ(q):
    absQ = np.sum(np.square(q))
    invq = np.array([-q[0], -q[1], -q[2], q[3]]) / absQ
    return invq

def q_inv(q):
    invq = np.array([-q[0], -q[1], -q[2], q[3]]) / q_norm(q)
    return invq

def q_mut(q1, q2):
    assert len(q1) == 4 == len(q2)
    a1, b1, c1, d1 = q1
    a2, b2, c2, d2 = q2
    
    qw = (a1 * a2 - b1 * b2 - c1 * c2 - d1 * d2)
    qx = (a1 * b2 + b1 * a2 + c1 * d2 - d1 * c2)
    qy = (a1 * c2 - b1 * d2 + c1 * a2 + d1 * b2)
    qz = (a1 * d2 + b1 * c2 - c1 * b2 + d1 * a2)
    
    return np.array([qw, qx, qy, qz])

def q_norm(q):
    return np.sum(np.square(q))

def quaternion2rm(q):
    q = q / np.linalg.norm(q)
    rm = np.zeros((3, 3))
    rm[0, 0] = 1 - 2 * q[2]**2 - 2 * q[3]**2
    rm[0, 1] = 2 * q[1] * q[2] - 2 * q[0] * q[3]
    rm[0, 2] = 2 * q[1] * q[3] + 2 * q[0] * q[2]

    rm[1, 0] = 2 * q[1] * q[2] + 2 * q[0] * q[3]
    rm[1, 1] = 1 - 2 * q[1]**2 - 2 * q[3]**2
    rm[1, 2] = 2 * q[2] * q[3] - 2 * q[0] * q[1]

    rm[2, 0] = 2 * q[1] * q[3] - 2 * q[0] * q[2]
    rm[2, 1] = 2 * q[2] * q[3] + 2 * q[0] * q[1]
    rm[2, 2] = 1 - 2 * q[1]**2 - 2 * q[2]**2

    return rm

def quaternion2xyz(q):
    q0 = q[:, 0:1]
    q1 = q[:, 1:2]
    q2 = q[:, 2:3]
    q3 = q[:, 3:]

    theta1 = np.arctan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1**2 + q2**2))
    theta2 = np.arcsin(2 * (q0 * q2 - q3 * q1))
    theta3 = np.arctan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2**2 + q3**3))
    
    return np.concatenate([theta1, theta2, theta3], axis=-1)    

def preprocess_point_cloud(pcd, voxel_size):
    # print(":: Downsample with a voxel size %.3f." % voxel_size)
    pcd_down = o3.voxel_down_sample(pcd, voxel_size)

    radius_normal = voxel_size * 2
    # print(":: Estimate normal with search radius %.3f." % radius_normal)
    o3.estimate_normals(pcd_down, search_param=o3.geometry.KDTreeSearchParamHybrid(
            radius = radius_normal, max_nn = 30) )

    radius_feature = voxel_size * 5
    # print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
    pcd_fpfh = o3.registration.compute_fpfh_feature(pcd_down,
             search_param=o3.geometry.KDTreeSearchParamHybrid(radius = radius_feature, max_nn = 100))
    return pcd_down, pcd_fpfh 

def prepare_dataset(voxel_size):
    # print(":: Load two point clouds and disturb initial pose.")
    source = o3.io.read_point_cloud("TestData/b_source5.pcd")
    target = o3.io.read_point_cloud("TestData/b_target.pcd")
    

    #trans_init = np.asarray([[0.0, 0.0, 1.0, 0.0],
    #                       [1.0, 0.0, 0.0, 0.0],
    #                        [0.0, 1.0, 0.0, 0.0],
    #                        [0.0, 0.0, 0.0, 1.0]])
    #source.transform(trans_init)
    # draw_registration_result(source, target, np.identity(4))

    source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)
    target_down, target_fpfh = preprocess_point_cloud(target, voxel_size)
    return source, target, source_down, target_down, source_fpfh, target_fpfh

def execute_global_registration(
        source_down, target_down, source_fpfh, target_fpfh, voxel_size):
    distance_threshold = voxel_size * 1.5
    # print(":: RANSAC registration on downsampled point clouds.")
    # print("   Since the downsampling voxel size is %.3f," % voxel_size)
    # print("   we use a liberal distance threshold %.3f." % distance_threshold)
    result = o3.registration.registration_ransac_based_on_feature_matching(
            source_down, target_down, source_fpfh, target_fpfh,
            distance_threshold,
            o3.registration.TransformationEstimationPointToPoint(False), 4,
            [o3.registration.CorrespondenceCheckerBasedOnEdgeLength(0.9),
            o3.registration.CorrespondenceCheckerBasedOnDistance(distance_threshold)],
            # RANSACConvergenceCriteria(4000000, 500))
            o3.registration.RANSACConvergenceCriteria(4000000, 10000))
    return result

def refine_registration(source, target, source_fpfh, target_fpfh, voxel_size):
    distance_threshold = voxel_size * 0.4
    # print(":: Point-to-plane ICP registration is applied on original point")
    # print("   clouds to refine the alignment. This time we use a strict")
    # print("   distance threshold %.3f." % distance_threshold)
    result = registration_icp(source, target, distance_threshold,
            result_ransac.transformation,
            TransformationEstimationPointToPoint())
    return result

def draw_registration_result(source, target, transformation):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    source_temp.paint_uniform_color([0, 0, 1]) # source point cloud: blue
    target_temp.paint_uniform_color([1, 0, 0]) # target point cloud: red
    source_temp.transform(transformation)
    o3.visualization.draw_geometries([source_temp, target_temp])

def rm2Quaternion(rm):
    qw = np.sqrt(1 + rm[0, 0] + rm[1, 1] + rm[2, 2]) / 2
    qx = (rm[2, 1] - rm[1, 2]) / (4 * qw)
    qy = (rm[0, 2] - rm[2, 0]) / (4 * qw)
    qz = (rm[1, 0] - rm[0, 1]) / (4 * qw)

    return np.array([qw, qx, qy, qz])

def rm2theta(rm):
    eps = 1e-16
    theta2 = np.arctan2(rm[0, 2], np.sqrt(np.power(rm[0, 0], 2) + np.power(rm[0, 1], 2)))
    theta1 = np.arctan2(-rm[1, 2]/ (np.cos(theta2)), rm[2, 2]/ (np.cos(theta2)) )
    theta3 = np.arctan2(-rm[0, 1]/ (np.cos(theta2)), rm[0, 0]/(np.cos(theta2)))
    
    theta1 = clip180(theta1)
    theta2 = clip90(theta2)
    theta3 = clip180(theta3)
    
    return np.array([theta1, theta2, theta3])

def rotate_with_q(xyz, q):
    xyz_tmp = np.append(xyz, 0)
    q_tmp = q_mut(q, q_mut(xyz_tmp, q_inv(q)))
    return q_tmp[:3]

def show_match_result(pred, tar, save=False, name=None):
    '''
    pcd shape must have dimension of number of pcd, 
    like (num, x, x, x, 3), (num, -1), (num, -1, 3)
    '''
    assert len(pred) == len(tar)
    fig = plt.figure(figsize=(12, 12))
    index = np.arange(len(tar))
    np.random.shuffle(index)
    index = index[:9]
    for i, idx in enumerate(index):
        data = tar[idx].reshape(-1, 3)
        x = data[:, 0]
        y = data[:, 1]
        z = data[:, 2]

        ax = fig.add_subplot(3, 3, i + 1, projection='3d')
        ax.scatter(x, y, z, c='r', marker='o')

        data = pred[idx].reshape(-1, 3)
        x = data[:, 0]
        y = data[:, 1]
        z = data[:, 2]
        ax.scatter(x, y, z, c='b', marker='o')

        ax.set_title(f'index: {idx}, RMS: {eva_rms(pred[idx], tar[idx]):.2f}')
    if save:
        if not Path('results').exists():
            Path('results').mkdir()
        plt.savefig(f'results/{name}_match_result.png')

def show_pcd_3d(x_data, save=False, name=None):
    fig = plt.figure(figsize=(12, 12))
    index = np.arange(len(x_data))
    np.random.shuffle(index)
    index = index[:9]
    for i, idx in enumerate(index):
        data = x_data[idx].reshape(-1, 3)
        x = data[:, 0]
        y = data[:, 1]
        z = data[:, 2]

        ax = fig.add_subplot(3, 3, i + 1, projection='3d')
        ax.scatter(x, y, z, c='r', marker='o')
        ax.set_title(idx)
    if save:
        if not Path('results').exists():
            Path('results').mkdir()
        plt.savefig(f'results/{name}_pcd.png')

def theta2rm(theta):   
    theta1, theta2, theta3 = theta
    
    rm = np.zeros((3,3))
    rm[0,0] = math.cos(theta2) * math.cos(theta3)
    rm[0,1] = -math.cos(theta2) * math.sin(theta3)
    rm[0,2] = math.sin(theta2)
    
    rm[1,0] = math.cos(theta1) * math.sin(theta3) + math.cos(theta3) * math.sin(theta1) * math.sin(theta2)
    rm[1,1] = math.cos(theta1) * math.cos(theta3) - math.sin(theta1) * math.sin(theta2) * math.sin(theta3)
    rm[1,2] = -math.cos(theta2) * math.sin(theta1)
    
    rm[2,0] = math.sin(theta1) * math.sin(theta3) - math.cos(theta1) * math.cos(theta3) * math.sin(theta2)
    rm[2,1] = math.cos(theta3) * math.sin(theta1) + math.cos(theta1) * math.sin(theta2) * math.sin(theta3)
    rm[2,2] = math.cos(theta1) * math.cos(theta2)

    return rm

def get_files_names(path):
    fnames=[]
    for idx, name in enumerate(path.ls()):
        if name.suffix == '.ply':
            fnames.append(name)
    return fnames

def np2pcd(np_pcd):
  pcd = o3.geometry.PointCloud()
  pcd.points = o3.utility.Vector3dVector(np_pcd)
  return pcd

# m is the distance ratio
def get_projection_point_cloud(whole_pcd, m, gamma, beta, alpha):
  shift = np.asarray([(np.max(whole_pcd[:,0])+np.min(whole_pcd[:,0]))/2, (np.max(whole_pcd[:,1])+np.min(whole_pcd[:,1]))/2, (np.max(whole_pcd[:,2])+np.min(whole_pcd[:,2]))/2])
  whole_pcd -= shift
  (xMax, yMax, zMax) = (np.max(whole_pcd[:,0]), np.max(whole_pcd[:,1]), np.max(whole_pcd[:,2]))
  (xMin, yMin, zMin) = (np.min(whole_pcd[:,0]), np.min(whole_pcd[:,1]), np.min(whole_pcd[:,2]))
  # print("xMax, yMax, zMax:" + str(xMax) + str(yMax) + str(zMax))
  # print("xMin, yMin, zMin:" + str(xMin) + str(yMin) + str(zMin))

  distance = xMax - xMin

  objCenter_x = (xMax + xMin)/2
  objCenter_y = (yMax + yMin)/2
  objCenter_z = (zMax + zMin)/2
  # print((objCenter_x, objCenter_y, objCenter_z))

  cameraCenter_x = objCenter_x - distance*5
  cameraCenter_y = objCenter_y + distance*5
  cameraCenter_z = objCenter_z - distance*5
  # print((cameraCenter_x, cameraCenter_y, cameraCenter_z))

  vecView = np.array([cameraCenter_x-objCenter_x, cameraCenter_y-objCenter_y, cameraCenter_z-objCenter_z])

  vecView_xy = np.array([vecView[0], vecView[1], 0])
  scale = np.linalg.norm(vecView_xy)
  vecView_xy /= scale
  # print(vecView_xy)

  vecView_xz = np.array([vecView[0], 0, vecView[2]])
  scale = np.linalg.norm(vecView_xz)
  vecView_xz /= scale

  vecView_yz = np.array([0, vecView[1], vecView[2]])
  scale = np.linalg.norm(vecView_yz)
  vecView_yz /= scale

  # e1 = np.array([1, 0, 0])
  # e2 = np.array([0, 1, 0])
  # e3 = np.array([0, 0, 1])

  # gamma = acos(np.dot(vecView_xy, e1))
  # print("gamma = " + str(gamma*(180/pi)))

  # beta = -acos(np.dot(vecView_xz, e3))
  # print("beta  = " + str(beta*(180/pi)))

  # alpha = -acos(np.dot(vecView_yz, e2))
  # print("alpha  = " + str(alpha*(180/pi)))

  Rot = np.array([[ cos(gamma)*cos(beta), -sin(gamma)*cos(alpha)+cos(gamma)*sin(beta)*sin(alpha),  sin(gamma)*sin(alpha)+cos(gamma)*sin(beta)*cos(alpha)], 
                  [ sin(gamma)*cos(beta),  cos(gamma)*cos(alpha)+sin(gamma)*sin(beta)*sin(alpha), -cos(gamma)*sin(alpha)+sin(gamma)*sin(beta)*cos(alpha)], 
                  [-sin(beta),             cos(beta)*sin(alpha),                                   cos(beta)*cos(alpha)]])
  # print(Rot)
  vecView = np.matmul(Rot, vecView)
  for i in range(whole_pcd.shape[0]):
      whole_pcd[i] = np.matmul(Rot, whole_pcd[i])

  (xMax, yMax, zMax) = (np.max(whole_pcd[:,0]), np.max(whole_pcd[:,1]), np.max(whole_pcd[:,2]))
  (xMin, yMin, zMin) = (np.min(whole_pcd[:,0]), np.min(whole_pcd[:,1]), np.min(whole_pcd[:,2]))

  # print((xMax, yMax, zMax))
  # print((xMin, yMin, zMin))
  
  xDist = (xMax - xMin)/m
  yDist = (yMax - yMin)/m
  zDist = (zMax - zMin)/m
  subcubes = [[[[] for i in range(m)] for j in range(m)] for k in range(m)]

  for points in range(whole_pcd.shape[0]):
      index_x = int((whole_pcd[points][0] - xMin) // xDist)
      index_y = int((whole_pcd[points][1] - yMin) // yDist)
      index_z = int((whole_pcd[points][2] - zMin) // zDist)
      
      if(index_x == m):
          index_x = m-1
      if(index_y == m):
          index_y = m-1
      if(index_z == m):
          index_z = m-1
      subcubes[index_z][index_y][index_x].append(whole_pcd[points].tolist())
  count = 0
  for z in range(m):
      for y in range(m):
          for x in range(m):
              count += len(subcubes[z][y][x])
  assert(count == whole_pcd.shape[0])
  surfacePoint = []
  for x in range(m):
      for y in range(m):
          for z in range(m-1, -1, -1):
              if(len(subcubes[z][y][x]) != 0):
                  surfacePoint.append(subcubes[z][y][x])
                  break
  test = []
  for i in range(len(surfacePoint)):
      for j in range(len(surfacePoint[i])):
          test.append(surfacePoint[i][j])
  return np.asarray(test)

def fpfh_feature(pcd_down, voxel_size):

    radius_normal = voxel_size * 2
    # print(":: Estimate normal with search radius %.3f." % radius_normal)
    pcd_down.estimate_normals(search_param=o3.geometry.KDTreeSearchParamHybrid(
            radius = radius_normal, max_nn = 30) )
    #o3.estimate_normals(pcd_down, search_param=o3.geometry.KDTreeSearchParamHybrid(
    #        radius = radius_normal, max_nn = 30) )

    radius_feature = voxel_size * 5
    # print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
    pcd_fpfh = o3.registration.compute_fpfh_feature(pcd_down,
             search_param=o3.geometry.KDTreeSearchParamHybrid(radius = radius_feature, max_nn = 100))
    return pcd_fpfh 

def cnn_model(input_shape=(12, 12, 12, 3)):
    inputs = Input(shape=input_shape)
    
    #  第一層
    C1 = Conv3D(32, (5, 5, 5), padding='same', name='conv3d_1', kernel_initializer='glorot_normal')(inputs)
    C1 = BatchNormalization(name='batch_normalization_c1')(C1)
    C11 = Activation('relu')(C1)
    
    # 第二層
    C1 = Conv3D(32, (3, 3, 3), padding='same', name='conv2d_2', kernel_initializer='glorot_normal')(C11)
    C1 = BatchNormalization(name='batch_normalization_c2')(C1)    
    C12 = Activation('relu')(C1)   
    
    # 接起來 1、2層，因為把Feature傳下去，效果為Loss function會變平滑，在硬體可以承受的範圍內可以無限加深、加速。Deeper and Deeper.
    Concat1 = concatenate([C11, C12])
    
    # 1*1*1的原因是因為接起來的參數太多，所以要壓扁，至於為什麼要這樣做自己去讀論文。
    Concat1 = Conv3D(16, (1, 1, 1), padding='same', name='conv3d_1x1_1', kernel_initializer='glorot_normal')(Concat1)
    # C1 = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(Concat1)
    
    # 第三層
    C2 = Conv3D(64, (5, 5, 5), padding='same', name='conv3d_3', kernel_initializer='glorot_normal')(Concat1)
    C2 = BatchNormalization(name='batch_normalization_c3')(C2)
    C21 = Activation('relu')(C2)   
    
    # 接起來 壓扁
    Concat2 = concatenate([Concat1, C21])
    Concat2 = Conv3D(32, (1, 1, 1), padding='same', name='conv3d_1x1_2', kernel_initializer='glorot_normal')(Concat2)
    
    # 第四層
    C2 = Conv3D(64, (3, 3, 3), padding='same', name='conv3d_4', kernel_initializer='glorot_normal')(Concat2)
    C2 = BatchNormalization(name='batch_normalization_c4')(C2)
    C22 = Activation('relu')(C2)    

    # 接起來 壓扁
    Concat3 = concatenate([Concat2, C22])
    Concat3 = Conv3D(32, (1, 1, 1), padding='same', name='conv3d_1x1_3', kernel_initializer='glorot_normal')(Concat3)    
    
    C2 = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(Concat3)
    
    # Global Average pooling3D 現在默認都用這個，原因自己查。
    C = GlobalAveragePooling3D(name='G_ave_pol')(C2)                         
    # C = Flatten()(C2)
    
    FC5 = Dense(256, activation='relu', name='dense_1', kernel_initializer='glorot_normal')(C)
    FC5 = BatchNormalization(name='batch_normalization_d1')(FC5)
    FC5 = Activation('relu')(FC5)
    # FC5 = Dropout(0.15)(FC5)
    
    Output = Dense(3, activation='relu', name='dense_3', kernel_initializer='glorot_normal')(FC5)
        
    model = Model(inputs=inputs, outputs=Output)
    
    return model

TRAIN_QUANTITY = 50000
TEST_QUANTITY = 9000
mSize = 12

voxel_size = 10
pcd = o3.io.read_point_cloud("data_generator/data/ori/armadillo/armadillo.ply")
downpcd = pcd.voxel_down_sample(voxel_size=voxel_size)
print(downpcd)

ori_pcd = np.asarray(pcd.points)
whole_pcd = np.asarray(pcd.points)

starttime = datetime.datetime.now()
data = np.asarray(downpcd.points) 

train_raw = []
test_raw = []

for i in range(TRAIN_QUANTITY + TEST_QUANTITY):
    q = np.random.uniform(0., 1., (3))        
    q = np.append(q, 0.)

    labels = q
    
    rotateData = np.zeros(data.shape)
    for n in range(data.shape[0]):
        rotateData[n] = rotate_with_q(data[n], q)
    # ********** Transformation Partials**********  
    # rm = quaternion2rm(q)
    # angle = rm2theta(rm)


    # rotateData = get_projection_point_cloud(rotateData, 79, angle[2], angle[1], angle[0])
    
  
    # ********** Transformation Partials**********  

    # core = getCore(rotateData)
    # rotateData = dataShift(rotateData, core).reshape(1, -1)       

    rotateData = rotateData.reshape(1, -1)

    dis = getAABBSize(rotateData)
    result = get3DMatrix(dis[0], dis[1], dis[2], rotateData, mSize).reshape(-1)

    # ##compute fpfh feature
    # xyz = result.reshape(np.int32(result.shape[0]/3),3)
    # pcd = np2pcd(xyz)

    # pcd_fpfh = fpfh_feature(pcd,voxel_size)
    # ##compute average weights
    # fpfh = pcd_fpfh.data.transpose()
    # average3 = np.zeros((fpfh.shape[0],3)) 
    # value = np.arange(1, 12)
    # for j in range(fpfh.shape[0]):
    #     theta = fpfh[j,:11].flatten()
    #     theta = np.average(value, weights = theta)
    #     average3[j][0] = theta

    #     fi = fpfh[j,11:22].flatten()
    #     fi =  np.average(value, weights = fi)
    #     average3[j][1] = fi

    #     arfa = fpfh[j,22:].flatten()
    #     arfa = np.average(value, weights= arfa)
    #     average3[j][2] = arfa

    # result = average3.flatten()
    tmp1 = np.hstack([result, labels])

    if i < TRAIN_QUANTITY:
        train_raw.append(tmp1.tolist())
    else:
        if i == TRAIN_QUANTITY:
            np.savetxt("Armadillo_train_" + str(TRAIN_QUANTITY) + "_Quaternion.txt", 
                        np.array(train_raw))
            # print("Armadillo_train_" + str(TRAIN_QUANTITY) + "_Quaternion.txt")
            train_raw = np.array(train_raw)
            gc.collect()
            

        test_raw.append(tmp1.tolist())


    if i % 200 == 0:
        endtime = datetime.datetime.now()
        print("item %d execute time: %s" % (i, endtime - starttime))


np.savetxt("Armadillo_test_" + str(TEST_QUANTITY) + "_Quaternion.txt", 
        np.array(test_raw))
# print("Armadillo_test_" + str(TEST_QUANTITY) + "_Quaternion.txt")
test_raw = np.array(test_raw)
gc.collect()
    
endtime = datetime.datetime.now()        
print ("Total execute time: " + str(endtime - starttime))     

model = cnn_model()

model.summary()

model_dir = "YK_registration_training/models/"
if not os.path.exists(model_dir):
    os.makedirs(model_dir)

reduce_lr = ReduceLROnPlateau(monitor='val_mean_absolute_error', factor=0.5 ,patience=4, min_lr=1e-6, verbose=True)

# model.compile(optimizer=Adam(3e-4, decay=3e-5, amsgrad=True, clipnorm=1.), loss='mse', metrics=['accuracy', 'mse', 'mae']) 
model.compile(optimizer=SGD(3e-3, decay= 1e-7, momentum=0.9, nesterov=True), loss='mae', metrics=['accuracy', 'mse', 'mae']) 
# model.compile(optimizer=Adadelta(), loss='mse', metrics=['accuracy', 'mse', 'mae'])

history = model.fit(x = x_train, y = y_train[:, :3], batch_size = 64, epochs=200, verbose=1,
                    callbacks=[reduce_lr],
                    validation_data=(x_dev, y_dev[:, :3]))

result = model.evaluate(x_train[:1000], y_train[:1000, :3] )
print('Accuracy of training Set:',result[1])
print('Loss of training set:', result[0])
print()
result_dev = model.evaluate(x_dev, y_dev[:, :3])
print('Accuracy of development set: ',result_dev[1])
print('Loss of development set:', result_dev[0])

model.save(model_dir+'Best-model.h5')
model = load_model(model_dir+'Best-model.h5')

if not os.path.exists('results'):
    os.makedirs('results')

plt.plot( history.history['loss'], color = 'blue', label = 'loss')
plt.plot( history.history['val_loss'], color = 'red', label = 'val_loss')


plt.legend(loc="best")
plt.ylabel('loss')
plt.xlabel('Epoch')
#plt.savefig(f'results/{fname.stem}_loss.png')
plt.show()

plt.plot( history.history['accuracy'], color = 'blue', label = 'acc')
plt.plot( history.history['val_accuracy'], color = 'red', label = 'val_acc')


plt.legend(loc="best")
plt.ylabel('acc')
plt.xlabel('Epoch')
#plt.savefig(f'results/{fname.stem}_acc.png')
plt.show()

result_test = model.evaluate(x_test, y_test[:, :3])
print('Accuracy of training Set:',result_test[1])
print('Loss of training set:', result_test[0])

starttime = datetime.datetime.now()
y_pred = model.predict(x_test)
y_pred = np.concatenate([y_pred, np.full((len(y_test), 1), 0.)], axis=-1)
endtime = datetime.datetime.now()

print("Prediction %d samples execute time: %s sec" % (len(x_test), (endtime - starttime).total_seconds())) 
print("Average prediction execute time: %f ms" % ((endtime - starttime).total_seconds() / len(x_test) * 1000)) 

successedT1 = 0
successedT2 = 0
successedT3 = 0
successedT4 = 0

maxofMax = 0
sumofMaxErr = 0
pred = None
errlist = []

for i in range(len(x_test)):
    
    if y_pred[i].any() == False:
        pred = np.full(3, 1e-16)
        pred = np.append(pred, 0.)
    else:
        pred = y_pred[i]
        
    estrm = quaternion2rm(pred)
    lbrm = quaternion2rm(y_test[i])
    errrm = estrm @ np.transpose(lbrm)
    
    err = rm2theta(errrm)
        
    maxErr = np.abs(np.max(err) / 3.14 * 180)


    
    errlist.append(maxErr)
    
    if maxErr > maxofMax:
        maxofMax = maxErr

    if maxErr < 0.5:
        successedT1 = successedT1 + 1
    if maxErr < 1:
        successedT2 = successedT2 + 1
    if maxErr < 1.75:
        successedT3 = successedT3 + 1
    if maxErr < 2.5:
        successedT4 = successedT4 + 1


    print(maxErr)
    sumofMaxErr = sumofMaxErr + maxErr

print('Max degree error: %.4f degrees' % maxofMax)
print('Average degree error: %.4f degrees' % (sumofMaxErr / len(y_test)))
print(' T1: %.2f \n T2: %.2f \n T3: %.2f \n T4: %.2f \n' 
      % (successedT1 / len(y_test) * 100, 
         successedT2 / len(y_test) * 100, 
         successedT3 / len(y_test) * 100, 
         successedT4 / len(y_test) * 100))

print("Prediction %d samples execute time: %s sec" % (len(x_test), (endtime - starttime).total_seconds())) 
print("Average prediction execute time: %f ms" % ((endtime - starttime).total_seconds() / len(x_test) * 1000)) 


def show_pcd_3d(x_data, save=False, name='123'):
    fig = plt.figure(figsize=(12, 12))
    index = np.arange(len(x_data))
    np.random.shuffle(index)
    index = index[:9]
    for i, idx in enumerate(index):
        data = x_data[idx].reshape(-1, 3)
        x = data[:, 0]
        y = data[:, 1]
        z = data[:, 2]

        ax = fig.add_subplot(3, 3, i + 1, projection='3d')
        ax.scatter(x, y, z, c='r', marker='o')
        ax.set_title(idx)
    if save:
        if not Path('results').exists():
            Path('results').mkdir()
        plt.savefig(f'results/{name}_pcd.png')

def scalar_reduction(x_hat, x_max, x_min):
    reduction = x_hat * (x_max - x_min) + x_min
    return np.float32(reduction)

# x_test_no_scalar = scalar_reduction(x_test, x_train_max, x_train_min)

# fig = plt.figure()
# ax = Axes3D(fig)

# x = data[:, 0]
# y = data[:, 1]
# z = data[:, 2]

# ax.scatter(x, y, z, c='r', marker='o')

# x_test_sample = x_test_no_scalar[0].reshape(-1, 3)
# x = x_test_sample[:, 0]
# y = x_test_sample[:, 1]
# z = x_test_sample[:, 2]
# ax.scatter(x, y, z, c='b', marker='o')
# ax.set_title('original')

# x_test_est = []
# data = np.asarray(downpcd.points)
# rotateData = data.copy()

# for i in range(len(x_test)):
#     if y_pred[i].any() == False:
#         pred = np.full(3, 1e-16)
#         pred = np.append(pred, 0.)
#     else:
#         pred = y_pred[i]
        
#     rotateData = data.copy()
#     for n in range(len(data)):
#         rotateData[n] = rotate_with_q(data[n], pred)
        
#     rotateData = rotateData.reshape(1, -1)
          
#     dis = getAABBSize(rotateData)

#     result = get3DMatrix(dis[0], dis[1], dis[2], rotateData, mSize).reshape(-1)
    
#     x_test_est.append(result.tolist())
    
# x_test_est = np.array(x_test_est).reshape(len(x_test), -1, 3)


# np.random.seed(0)
# show_pcd_3d(x_test_no_scalar, save=True, name=fname.stem)

# fig = plt.figure()
# ax = Axes3D(fig)
# data_ = x_test_no_scalar[5].reshape(-1, 3)

# x = data_[:, 0]
# y = data_[:, 1]
# z = data_[:, 2]

# ax.scatter(x, y, z, c='r', marker='o')


# data_ = x_test_est[5].reshape(-1, 3)

# x = data_[:, 0]
# y = data_[:, 1]
# z = data_[:, 2]

# ax.scatter(x, y, z, c='b', marker='o')
# ax.set_title('register')

# rms = []
# for i in range(len(x_test)):
#     rms.append(eva_rms(x_test_no_scalar[i], x_test_est[i]))

# print(f'Max RMS of {fname.stem} testing data: {np.max(rms):.2f}')
# print(f'Min RMS of {fname.stem} testing data: {np.min(rms):.2f}')
# print(f'Mean RMS of {fname.stem} testing data: {np.mean(rms):.2f}')