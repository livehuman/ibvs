import numpy as np

from .q_mut import q_mut
from .q_norm import q_norm
from .q_inv import q_inv


def rotate_with_q(xyz, q):
    xyz_tmp = np.append(xyz, 0)
    q_tmp = q_mut(q, q_mut(xyz_tmp, q_inv(q)))
    return q_tmp[:3]
