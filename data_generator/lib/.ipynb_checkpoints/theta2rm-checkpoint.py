import numpy as np
import math

def theta2rm(theta):   
    theta1, theta2, theta3 = theta
    
    rm = np.zeros((3,3))
    rm[0,0] = math.cos(theta2) * math.cos(theta3)
    rm[0,1] = -math.cos(theta2) * math.sin(theta3)
    rm[0,2] = math.sin(theta2)
    
    rm[1,0] = math.cos(theta1) * math.sin(theta3) + math.cos(theta3) * math.sin(theta1) * math.sin(theta2)
    rm[1,1] = math.cos(theta1) * math.cos(theta3) - math.sin(theta1) * math.sin(theta2) * math.sin(theta3)
    rm[1,2] = -math.cos(theta2) * math.sin(theta1)
    
    rm[2,0] = math.sin(theta1) * math.sin(theta3) - math.cos(theta1) * math.cos(theta3) * math.sin(theta2)
    rm[2,1] = math.cos(theta3) * math.sin(theta1) + math.cos(theta1) * math.sin(theta2) * math.sin(theta3)
    rm[2,2] = math.cos(theta1) * math.cos(theta2)

    return rm