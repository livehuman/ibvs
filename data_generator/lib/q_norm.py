import numpy as np 

def q_norm(q):
    return np.sum(np.square(q))
