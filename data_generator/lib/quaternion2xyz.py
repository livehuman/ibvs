import numpy as np


def quaternion2xyz(q):
    q0 = q[:, 0:1]
    q1 = q[:, 1:2]
    q2 = q[:, 2:3]
    q3 = q[:, 3:]

    theta1 = np.arctan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1**2 + q2**2))
    theta2 = np.arcsin(2 * (q0 * q2 - q3 * q1))
    theta3 = np.arctan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2**2 + q3**3))
    
    return np.concatenate([theta1, theta2, theta3], axis=-1)    
