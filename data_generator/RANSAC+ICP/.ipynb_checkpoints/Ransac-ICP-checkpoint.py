from open3d import *
import numpy as np
import copy, time
from numpy import linalg as LA
import chart_studio.plotly as py
import plotly.graph_objs as go

def draw_registration_result(source, target, transformation):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    source_temp.paint_uniform_color([0, 0, 1]) # source point cloud: blue
    target_temp.paint_uniform_color([1, 0, 0]) # target point cloud: red
    source_temp.transform(transformation)
    draw_geometries([source_temp, target_temp])

def preprocess_point_cloud(pcd, voxel_size):
    # print(":: Downsample with a voxel size %.3f." % voxel_size)
    pcd_down = voxel_down_sample(pcd, voxel_size)

    radius_normal = voxel_size * 2
    # print(":: Estimate normal with search radius %.3f." % radius_normal)
    estimate_normals(pcd_down, KDTreeSearchParamHybrid(
            radius = radius_normal, max_nn = 30))

    radius_feature = voxel_size * 5
    # print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
    pcd_fpfh = compute_fpfh_feature(pcd_down,
            KDTreeSearchParamHybrid(radius = radius_feature, max_nn = 100))
    return pcd_down, pcd_fpfh

def prepare_dataset(voxel_size):
    # print(":: Load two point clouds and disturb initial pose.")
    source = read_point_cloud("TestData/b_source5.pcd")
    target = read_point_cloud("TestData/b_target.pcd")

    trans_init = np.asarray([[0.0, 0.0, 1.0, 0.0],
                            [1.0, 0.0, 0.0, 0.0],
                            [0.0, 1.0, 0.0, 0.0],
                            [0.0, 0.0, 0.0, 1.0]])
    source.transform(trans_init)
    # draw_registration_result(source, target, np.identity(4))

    source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)
    target_down, target_fpfh = preprocess_point_cloud(target, voxel_size)
    return source, target, source_down, target_down, source_fpfh, target_fpfh

def execute_global_registration(
        source_down, target_down, source_fpfh, target_fpfh, voxel_size):
    distance_threshold = voxel_size * 1.5
    # print(":: RANSAC registration on downsampled point clouds.")
    # print("   Since the downsampling voxel size is %.3f," % voxel_size)
    # print("   we use a liberal distance threshold %.3f." % distance_threshold)
    result = registration_ransac_based_on_feature_matching(
            source_down, target_down, source_fpfh, target_fpfh,
            distance_threshold,
            TransformationEstimationPointToPoint(False), 4,
            [CorrespondenceCheckerBasedOnEdgeLength(0.9),
            CorrespondenceCheckerBasedOnDistance(distance_threshold)],
            # RANSACConvergenceCriteria(4000000, 500))
            RANSACConvergenceCriteria(4000000, 10000))
    return result

def refine_registration(source, target, source_fpfh, target_fpfh, voxel_size):
    distance_threshold = voxel_size * 0.4
    # print(":: Point-to-plane ICP registration is applied on original point")
    # print("   clouds to refine the alignment. This time we use a strict")
    # print("   distance threshold %.3f." % distance_threshold)
    result = registration_icp(source, target, distance_threshold,
            result_ransac.transformation,
            TransformationEstimationPointToPoint())
    return result

if __name__ == "__main__":
    voxel_size = 0.005 # means 5mm for the dataset
    # start to count the computation time

    tStart = time.time()
    source, target, source_down, target_down, source_fpfh, target_fpfh = \
            prepare_dataset(voxel_size)

    result_ransac = execute_global_registration(source_down, target_down, source_fpfh, target_fpfh, voxel_size)

    # print(result_ransac)
    # draw_registration_result(source_down, target_down, result_ransac.transformation)
    result_icp = refine_registration(source, target, source_fpfh, target_fpfh, voxel_size)
    tStop = time.time()
    consumingtime = tStop - tStart
    print("Computation time: ", round(consumingtime,3), " (s)")


    # print(result_icp)
    source.transform(result_icp.transformation)  # transform source with respect to ICP transformation result
    # draw_registration_result(source, target, np.identity(4))

    temp_source = np.asarray(source.points)
    temp_target = np.asarray(target.points)

    # compute MSE
    min = 100  # initialize the value of min
    corr_pc = []  # create a new empty array
    length_source = temp_source.shape[0]
    length_target = temp_target.shape[0]

    for i in range(length_source):
        for j in range(length_target):
            # compute the distance between one point in source and one point in target
            temp_dist = LA.norm(np.subtract(temp_source[i, :], temp_target[j, :]))
            if min > temp_dist:
                min = temp_dist
                index = j
        corr_pc.append(temp_target[index, :])
        min = 100
    corr_pc = np.array(corr_pc)

    # print("Shape of Corresponding points: ", np.shape(corr_pc))

    # compute norm2 of each corresponding point
    norm = np.linalg.norm(np.subtract(temp_source, corr_pc), axis=1)
    # compute sum of all norm2
    sum = np.sum(norm)
    print("MSE = ", round((sum / length_source) * 1000, 3), " (mm)")



