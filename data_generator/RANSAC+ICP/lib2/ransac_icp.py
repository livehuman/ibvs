import open3d as o3
import numpy as np
import copy, time
from numpy import linalg as LA
import chart_studio.plotly as py
import plotly.graph_objs as go
import math

#import os
#import gc
#import math
#import datetime
#import numpy as np
#import pandas as pd
#import pylab
#import random
#import scipy
#import scipy.ndimage as nd
#import cv2
#import open3d as o3




def preprocess_point_cloud(pcd, voxel_size):
    # print(":: Downsample with a voxel size %.3f." % voxel_size)

    pcd_down = o3.voxel_down_sample(pcd, voxel_size)

    radius_normal = voxel_size * 2
    # print(":: Estimate normal with search radius %.3f." % radius_normal)
    o3.estimate_normals(pcd_down, search_param=o3.geometry.KDTreeSearchParamHybrid(
            radius = radius_normal, max_nn = 30) )

    radius_feature = voxel_size * 5
    # print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
    pcd_fpfh = o3.registration.compute_fpfh_feature(pcd_down,
             search_param=o3.geometry.KDTreeSearchParamHybrid(radius = radius_feature, max_nn = 100))
    return pcd_down, pcd_fpfh 

def prepare_dataset(voxel_size):
    # print(":: Load two point clouds and disturb initial pose.")
    source = o3.io.read_point_cloud("TestData/bunny/bunny.ply")
    target = o3.io.read_point_cloud("TestData/bunny/bunny/bun000.pcd")
    

    #trans_init = np.asarray([[0.0, 0.0, 1.0, 0.0],
    #                       [1.0, 0.0, 0.0, 0.0],
    #                        [0.0, 1.0, 0.0, 0.0],
    #                        [0.0, 0.0, 0.0, 1.0]])
    #source.transform(trans_init)
    # draw_registration_result(source, target, np.identity(4))

    source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)
    target_down, target_fpfh = preprocess_point_cloud(target, voxel_size)
    return source, target, source_down, target_down, source_fpfh, target_fpfh


def execute_global_registration(
        source_down, target_down, source_fpfh, target_fpfh, voxel_size):
    distance_threshold = voxel_size * 1.5
    # print(":: RANSAC registration on downsampled point clouds.")
    # print("   Since the downsampling voxel size is %.3f," % voxel_size)
    # print("   we use a liberal distance threshold %.3f." % distance_threshold)
    result = o3.registration.registration_ransac_based_on_feature_matching(
            source_down, target_down, source_fpfh, target_fpfh,
            distance_threshold,
            o3.registration.TransformationEstimationPointToPoint(False), 4,
            [o3.registration.CorrespondenceCheckerBasedOnEdgeLength(0.9),
            o3.registration.CorrespondenceCheckerBasedOnDistance(distance_threshold)],
            # RANSACConvergenceCriteria(4000000, 500))
            o3.registration.RANSACConvergenceCriteria(4000000, 10000))
    return result


def refine_registration(source, target, source_fpfh, target_fpfh, voxel_size):
    distance_threshold = voxel_size * 0.4
    # print(":: Point-to-plane ICP registration is applied on original point")
    # print("   clouds to refine the alignment. This time we use a strict")
    # print("   distance threshold %.3f." % distance_threshold)
    result = o3.registration.registration_icp(source, target, distance_threshold,
            result_ransac.transformation,
            o3.registration.TransformationEstimationPointToPoint())
    return result

def draw_registration_result(source, target, transformation):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    source_temp.paint_uniform_color([0, 0, 1]) # source point cloud: blue
    target_temp.paint_uniform_color([1, 0, 0]) # target point cloud: red
    source_temp.transform(transformation)
    o3.visualization.draw_geometries([source_temp, target_temp])


