import numpy as np

def rm2Quaternion(rm):
    qw = np.sqrt(1 + rm[0, 0] + rm[1, 1] + rm[2, 2]) / 2
    qx = (rm[2, 1] - rm[1, 2]) / (4 * qw)
    qy = (rm[0, 2] - rm[2, 0]) / (4 * qw)
    qz = (rm[1, 0] - rm[0, 1]) / (4 * qw)

    return np.array([qw, qx, qy, qz])