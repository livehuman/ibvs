import math
import numpy as np

def cal_3D_angles(point1, point2, origin):
    dir_x = point1 - origin
    dir_y = point2 - origin

    xy = dir_x[0] * dir_y[0] + dir_x[1] * dir_y[1] - dir_x[2] * dir_y[2] 
    abs_x = math.sqrt(dir_x[0]*dir_x[0] + dir_x[1]*dir_x[1] + dir_x[2]*dir_x[2])
    abs_y = math.sqrt(dir_y[0]*dir_y[0] + dir_y[1]*dir_y[1] + dir_y[2]*dir_y[2])
    angle = math.degrees(math.acos(xy/(abs_x*abs_y)))
    return angle

