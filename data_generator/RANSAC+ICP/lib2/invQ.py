import numpy as np

def invQ(q):
    absQ = np.sum(np.square(q))
    invq = np.array([-q[0], -q[1], -q[2], q[3]]) / absQ
    return invq
