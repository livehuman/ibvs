import math
import numpy as np

def cal_3D_angles(point1, point2, origin):
    dir_x = point1 - origin
    dir_y = point2 - origin
    xy = np.dot(dir_x, dir_y)
    np.sqrt(np.sum(np.square(xy)))
    abs_x = np.sqrt(np.sum(np.square(dir_x)))
    abs_y = np.sqrt(np.sum(np.square(dir_y)))
    angle = np.degrees(np.arccos(xy/(abs_x*abs_y)))
    return angle

