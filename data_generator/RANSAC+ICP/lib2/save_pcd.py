import numpy as np 
import open3d as o3 
import os 

def save_pcd(data, path, name, index, visualization):
    if not os.path.exists( path + "xyz"):
        os.makedirs( path + "xyz")

    if not os.path.exists( path + "pcd"):
        os.makedirs( path + "pcd")

    xyz = data.reshape(-1, 3) # get the points from mesh
    np.savetxt( path + "xyz" + "/" + name + str(index) + ".txt", xyz) # save files for txt (.stem means to clear the semifiles) 
    pcd = o3.io.read_point_cloud( path + "xyz" + "/" + name + str(index) + ".txt", format='xyz') 
    o3.write_point_cloud( path + "pcd" + "/" + name + str(index) + ".pcd", pcd)
    if visualization == True:
        o3.visualization.draw_geometries([pcd])
    else:
        pass