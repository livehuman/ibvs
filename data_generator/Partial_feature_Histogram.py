#%% Change working directory from the workspace root to the ipynb file location. Turn this addition off with the DataScience.changeDirOnImportExport setting
import os
try:
	os.chdir(os.path.join(os.getcwd(), 'data_generator'))
	print(os.getcwd())
except:
	pass
#%%

import os
import gc
import math
import datetime
import numpy as np
import pandas as pd
import pylab
import random
import scipy
import scipy.ndimage as nd

import cv2
import open3d as o3

from mpl_toolkits.mplot3d import Axes3D
from fastai.vision import Path

from lib.cal_3D_intersection_angle import cal_3D_intersection_angle
from lib.cal_3Dangle import cal_3D_angles
from lib.dashift import dataShift
from lib.eva_rms import eva_rms
from lib.gather_pcd import gather_pcd
from lib.get_files_names import get_files_names
from lib.get3DMatrix_AOV import get3DMatrix
from lib.getAABBSize import getAABBSize
from lib.getCore import getCore
from lib.getMaxRadius import getMaxRadius
from lib.invQ import invQ
from lib.q_inv import q_inv
from lib.q_mut import q_mut
from lib.q_norm import q_norm
from lib.quaternion2rm import quaternion2rm
from lib.quaternion2xyz import quaternion2xyz
from lib.rm2quaternion import rm2Quaternion
from lib.rm2theta import rm2theta
from lib.rotate_with_q import rotate_with_q
from lib.save_pcd import save_pcd
from lib.show_match_result import show_match_result
from lib.show_pcd_3d import show_pcd_3d
from lib.theta2rm import theta2rm


#%%
TRAIN_QUANTITY = 2
TEST_QUANTITY = 2
mSize = 12
region_count = 700


#%%
# load and saveing path
print("Load path: \n")
print(os.listdir())
load_path = Path('data/ori/bunny')
load_path.ls(), len(load_path.ls())


#%%
save_path = Path('datasets/feature_map')
save_path.ls(), len(save_path.ls())
print("Save path:" + str(save_path))

#%%
load_p = get_files_names(load_path)
print(load_p)

save_p = get_files_names(save_path)
print(save_p)


#%%
load_fname = load_p[0]
load_fname.stem

#%% [markdown]
# ## Read points cloud
# region_count = 500
# Bunny = 0.006, 2150 points
# 

#%%
pcd = o3.io.read_point_cloud(str(load_fname))
downpcd = pcd.voxel_down_sample(voxel_size=0.003)
print(downpcd)
pcd_tree = o3.geometry.KDTreeFlann(downpcd)


#%%
ndownpcd = np.asarray(downpcd.points)
ndownpcd.shape


#%%
point_index = random.randint(0,ndownpcd.shape[0]-1) # Random the index point of kdtree to get differeant region of it
[k, idx, _] = pcd_tree.search_knn_vector_3d(downpcd.points[point_index], 5) # Get the region idx
partial_pcd =  gather_pcd(ndownpcd, idx) #Get partial points


#%%
print(partial_pcd[1])


#%%
point_index


#%%
ndownpcd[point_index]


#%%
dist = ndownpcd[point_index]-partial_pcd[1]


#%%
linq = math.sqrt(dist[0]*dist[0]+dist[1]*dist[1]+dist[2]*dist[2])


#%%
print(linq)

#%% [markdown]
# ## New feature with four nearest points


#%%
ndownpcd.shape[0]


#%%
for i in range(ndownpcd.shape[0]-1):
    [k, idx, _] = pcd_tree.search_knn_vector_3d(downpcd.points[i], 5) # Get the region idx
    partial_pcd =  gather_pcd(ndownpcd, idx) #Get partial points
    print("item" + str(i) + " : " + str(cal_3D_intersection_angle(partial_pcd)))


#%%
downpcd.estimate_normals(search_param=o3.geometry.KDTreeSearchParamHybrid(
        radius=0.006, max_nn=30))

#%%
np_normals = np.asarray(downpcd.normals)


#%%
np_normals.shape

#%%
np_normals.shape
print(np_normals[0])
print(downpcd.points[0])

#%%
o3.visualization.draw_geometries([downpcd])


#%%
def surface_curvature(X,Y,Z):

	(lr,lb)=X.shape

	print(lr)
	#print("awfshss-------------")
	print(lb)
#First Derivatives
	Xv,Xu=np.gradient(X)
	Yv,Yu=np.gradient(Y)
	Zv,Zu=np.gradient(Z)
#	print(Xu)

#Second Derivatives
	Xuv,Xuu=np.gradient(Xu)
	Yuv,Yuu=np.gradient(Yu)
	Zuv,Zuu=np.gradient(Zu)   

	Xvv,Xuv=np.gradient(Xv)
	Yvv,Yuv=np.gradient(Yv)
	Zvv,Zuv=np.gradient(Zv) 

#2D to 1D conversion 
#Reshape to 1D vectors
	Xu=np.reshape(Xu,lr*lb)
	Yu=np.reshape(Yu,lr*lb)
	Zu=np.reshape(Zu,lr*lb)
	Xv=np.reshape(Xv,lr*lb)
	Yv=np.reshape(Yv,lr*lb)
	Zv=np.reshape(Zv,lr*lb)
	Xuu=np.reshape(Xuu,lr*lb)
	Yuu=np.reshape(Yuu,lr*lb)
	Zuu=np.reshape(Zuu,lr*lb)
	Xuv=np.reshape(Xuv,lr*lb)
	Yuv=np.reshape(Yuv,lr*lb)
	Zuv=np.reshape(Zuv,lr*lb)
	Xvv=np.reshape(Xvv,lr*lb)
	Yvv=np.reshape(Yvv,lr*lb)
	Zvv=np.reshape(Zvv,lr*lb)

	Xu=np.c_[Xu, Yu, Zu]
	Xv=np.c_[Xv, Yv, Zv]
	Xuu=np.c_[Xuu, Yuu, Zuu]
	Xuv=np.c_[Xuv, Yuv, Zuv]
	Xvv=np.c_[Xvv, Yvv, Zvv]

#% First fundamental Coeffecients of the surface (E,F,G)
	
	E=np.einsum('ij,ij->i', Xu, Xu) 
	F=np.einsum('ij,ij->i', Xu, Xv) 
	G=np.einsum('ij,ij->i', Xv, Xv) 

	m=np.cross(Xu,Xv,axisa=1, axisb=1) 
	p=np.sqrt(np.einsum('ij,ij->i', m, m)) 
	n=m/np.c_[p,p,p]
# n is the normal
#% Second fundamental Coeffecients of the surface (L,M,N), (e,f,g)
	L= np.einsum('ij,ij->i', Xuu, n) #e
	M= np.einsum('ij,ij->i', Xuv, n) #f
	N= np.einsum('ij,ij->i', Xvv, n) #g

# Alternative formula for gaussian curvature in wiki 
# K = det(second fundamental) / det(first fundamental)
#% Gaussian Curvature
	K=(L*N-M**2)/(E*G-F**2)
	K=np.reshape(K,lr*lb)
#	print(K.size)
#wiki trace of (second fundamental)(first fundamental inverse)
#% Mean Curvature
	H = ((E*N + G*L - 2*F*M)/((E*G - F**2)))/2
	print(H.shape)
	H = np.reshape(H,lr*lb)
#	print(H.size)

#% Principle Curvatures
	Pmax = H + np.sqrt(H**2 - K)
	Pmin = H - np.sqrt(H**2 - K)
#[Pmax, Pmin]
	Principle = [Pmax,Pmin]
	return Principle
#s = nd.gaussian_filter(z,10)
x = scipy.linspace(-1,1,3)
y = scipy.linspace(-1,1,3)
[x,y]=scipy.meshgrid(x,y)
z = (x**3 +y**2 +x*y)
print("x.shape" + str(x.shape))
print("y.shape" + str(y.shape))
print("z.shape" + str(z.shape))

print("x: " + str(x))
print("y: " + str(y))
print("z: " + str(z))
s = nd.gaussian_filter(z,10)
print(partial_pcd[:, 0].shape)
temp1 = surface_curvature(x, y, z)
# temp1 = surface_curvature(ndownpcd[:, 0].reshape(ndownpcd[0], 3), ndownpcd[:, 1].reshape(ndownpcd[0], 3), ndownpcd[:, 2].reshape(ndownpcd[0], 3))
print("maximum curvatures")
print(temp1[0])
print("minimum curvatures")
print(temp1[1])
fig = pylab.figure()
ax = Axes3D(fig)

# ax.plot_surface(ndownpcd[:, 0], ndownpcd[:, 1], ndownpcd[:, 2])
ax.plot_surface(x,y,z)
pylab.show()

#%% [markdown]
# ## check point clouds data

#%%
np.savetxt( f'{save_path}\\train_data\ori_test.txt', partial_pcd) # save files for txt (.stem means to clear the semifiles) 
pcd = o3.io.read_point_cloud(f'{save_path}\\train_data\ori_test.txt', format='xyz')    
o3.visualization.draw_geometries([pcd])


#%%
starttime = datetime.datetime.now()

data = partial_pcd

train_raw = []
test_raw = []
for i in range(TRAIN_QUANTITY + TEST_QUANTITY):
    q = np.random.uniform(0., 1., (3))        
    q = np.append(q, 0.)

    labels = q
    
    rotateData = np.zeros(data.shape)
    for n in range(data.shape[0]):
        rotateData[n] = rotate_with_q(data[n], q)
    
    rotateData = rotateData.reshape(1, -1)

    dis = getAABBSize(rotateData)
    result = get3DMatrix(dis[0], dis[1], dis[2], rotateData, 12).reshape(-1)

    tmp1 = np.hstack([result, labels])
    if i < TRAIN_QUANTITY:
        train_raw.append(tmp1.tolist())
    else:
        if i == TRAIN_QUANTITY:
            np.savetxt(f'{save_path}\\train_data\{load_fname.stem}_train_{TRAIN_QUANTITY}.txt', np.array(train_raw))
            print(load_fname.stem + str(TRAIN_QUANTITY) + ".txt")
            train_raw = np.array(train_raw)
            gc.collect()

        test_raw.append(tmp1.tolist())

    if i % 200 == 0:
        endtime = datetime.datetime.now()
        print("item %d execute time: %s" % (i, endtime - starttime))

np.savetxt(f'{save_path}\\dev_data\{load_fname.stem}_test_{TEST_QUANTITY}.txt', 
        np.array(test_raw))
print(load_fname.stem + str(TEST_QUANTITY) + ".txt")
test_raw = np.array(test_raw)
gc.collect()

endtime = datetime.datetime.now()        
print ("Total execute time: " + str(endtime - starttime)) 


#%%



#%%



