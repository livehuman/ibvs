import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt(r'D:\NTUT\Research\data\4pipe\CNN_3D_train_rotate_3axis_feature_Obj3_Onehot.txt')

XY = data[::3, 0 : data.shape[1] - 4]
XZ = data[1::3, 0 : data.shape[1] - 4]
YZ = data[2::3, 0 : data.shape[1] - 4]

print (XY.shape)
print (XZ.shape)
print (YZ.shape)

index = 1

XYIndex = XY[index, :].reshape(28, 28)
XZIndex = XZ[index, :].reshape(28, 28)
YZIndex = YZ[index, :].reshape(28, 28)

plt.ion()
fig = plt.figure()
ax = fig.add_subplot(131)
ax.imshow(XYIndex)
plt.title('XY')
ax1 = fig.add_subplot(132)
ax1.imshow(XZIndex)
plt.title('XZ')
ax2 = fig.add_subplot(133)
ax2.imshow(YZIndex)
plt.title('YZ')
fig.show()
plt.ioff()
input()
