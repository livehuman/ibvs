import numpy as np 
import math

def cal_3D_angles(point1, point2, origin):
    dir_x = point1 - origin
    dir_y = point2 - origin
    xy = np.dot(dir_x, dir_y)
    np.sqrt(np.sum(np.square(xy)))
    abs_x = np.sqrt(np.sum(np.square(dir_x)))
    abs_y = np.sqrt(np.sum(np.square(dir_y)))
    angle = np.degrees(np.arccos(xy/(abs_x*abs_y)))
    return angle

def cal_3D_intersection_angle(partial_pcd, region_number):
    angles = []

    for w in range(region_number-1):
        if(w+1 < region_number-1):
            angles.append(cal_3D_angles(partial_pcd[w+1], partial_pcd[w+1+1], partial_pcd[0]))
        else:
            angles.append(cal_3D_angles(partial_pcd[region_number-1], partial_pcd[1], partial_pcd[0]))
        
    return angles
