import numpy as np
from functools import partial

clip180 = partial(np.clip, a_min=-3.14, a_max=3.14)
clip90 = partial(np.clip, a_min=0., a_max=1.57)

def rm2theta(rm):
    eps = 1e-16
    theta2 = np.arctan2(rm[0, 2], np.sqrt(np.power(rm[0, 0], 2) + np.power(rm[0, 1], 2)))
    theta1 = np.arctan2(-rm[1, 2]/ (np.cos(theta2)), rm[2, 2]/ (np.cos(theta2)) )
    theta3 = np.arctan2(-rm[0, 1]/ (np.cos(theta2)), rm[0, 0]/(np.cos(theta2)))
    
    theta1 = clip180(theta1)
    theta2 = clip90(theta2)
    theta3 = clip180(theta3)
    
    return np.array([theta1, theta2, theta3])
