import numpy as np 

def get_files_names(path):
    fnames=[]
    for idx, name in enumerate(path.ls()):
        if name.suffix == '.ply':
            fnames.append(name)
    return fnames